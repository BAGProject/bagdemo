package testcases;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.relevantcodes.extentreports.LogStatus;

import core.BrowserFactory;
import core.Lib;
import core.Reporter;
import common.Web;

public class TestBase extends Web{

	public static WebDriver driver = null;
	public Reporter report = null;
	public Web web;
	public String scr;
	
	@BeforeSuite
	public void loadConfig(){
			Lib.loadTestProperties();
			report = new Reporter();
	}
			
	@BeforeClass
	public void initDriver(){
		
		driver = BrowserFactory.getDriver(Lib.getProperty("BROWSER_NAME"));
		
	} 
	
	@AfterClass
	public void quitDriver(){
		BrowserFactory.quitDriver();
	}
	
	@BeforeMethod
	public void initData(Method m) {
		report.StartTest(m.getName());
	}
	
	public void logReport(LogStatus status, String testStep, String stepDetails) {
		report.Log(status, testStep, stepDetails);
		}
	
	public static void captureScreenshot(WebDriver driver) throws IOException{
		takeScreenshot(driver,"error");
	}
	
	
	@AfterMethod
	
	public void afterTest() {
		report.EndTest();
	}
	
	@AfterSuite
	public void cleanTest() {
		report.FlushTest();
		report.CleanTest();
	}
	
}
