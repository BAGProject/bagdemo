package testcases;

import java.io.IOException;

import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import common.Web;
import core.Lib;
import pages.HomePage;
import pages.PersnDetPage;
import pages.ProductDetPage;
import pages.BillingAddressPage;
import pages.CartPage;
import pages.SupercarExpPage;

public class TestSuite extends TestBase{
	
	HomePage home = null;
	SupercarExpPage supercar = null;
	ProductDetPage proddet = null;
	CartPage cart = null;
	PersnDetPage info=null;
	BillingAddressPage bill=null;
	
	
	@Test
	public void TC_Buy_a_Product() throws IOException{
		String prodVal;
		
		captureScreenshot(driver);
		home = new HomePage(driver);
		
		//User can open an Home Page
		
		home.launchApp(Lib.getProperty("APP_URL"));
		if(home.pageTitle(driver).equals("Experience Days and Gifts from Buyagift")){
			logReport(LogStatus.PASS,"Verify if user can open home page","Verification succesfull");
		}
		else{
			logReport(LogStatus.FAIL,"Verify if user can open home page","Verification failed");
			captureScreenshot(driver);
		}
		
		//A user can find (either by search or by menu) the product of interest
		
		supercar = home.navToSupercarExp();
		if(supercar.pageTitle(driver).equals("Supercar Experiences | The World&#39;s Most Powerful Cars")){
			logReport(LogStatus.PASS,"Verify if user can search through menu Driving and click supercars ","Verification succesfull");
		}
		else{
			logReport(LogStatus.FAIL,"Verify if user can search through menu Driving and click supercars","Verification failed");
			//captureScreenshot(driver);
		}
		
		//The correct price is displayed on the product page
		
		prodVal = supercar.getProdVal().replaceAll("[^\\d.]","");
		proddet = supercar.selectProduct();
		if(prodVal.equals(proddet.getCurrProdPrice().replaceAll("[^\\d.]",""))){
			logReport(LogStatus.PASS,"Verify if current product price is same as selected product price","Verification succesfull");
		}else{
			logReport(LogStatus.FAIL,"Verify if current product price is same as selected product price","Verification failed");
			captureScreenshot(driver);
		}
		//The user can add the product to their basket
		
		cart = proddet.clickBuyNow();
		if(cart.pageTitle(driver).equals("BAG Basket Page")){
			logReport(LogStatus.PASS,"Verify if user can add the product to their basket ","Verification succesfull");
		}
		else{
			logReport(LogStatus.FAIL,"Verify if user can add the product to their basket","Verification failed");
			captureScreenshot(driver);
		}
		
		//The user can add a personalized message to the product in their basket
			
		cart.addPerMessage("Have a safe drive");
		if(cart.pageTitle(driver).equals("BAG Basket Page")){
			logReport(LogStatus.PASS,"Verify if user can add a personalized message to the product in their basket ","Verification succesfull");
		}
		else{
			logReport(LogStatus.FAIL,"Verify if user can add a personalized message to the product in their basket","Verification failed");
			captureScreenshot(driver);
		}
		
		//The user can select E-Voucher delivery method
		
		cart.addDeliveryMethod("E-voucher (Free)");
		if(cart.pageTitle(driver).equals("BAG Basket Page")){
			logReport(LogStatus.PASS,"Verify if user can select E-Voucher delivery method ","Verification succesfull");
		}
		else{
			logReport(LogStatus.FAIL,"Verify if user can select E-Voucher delivery method","Verification failed");
			captureScreenshot(driver);
		}
		
		//No additional delivery charge is added when E-Voucher is selected
			
		if(cart.getDeliChgsVal().equals("0.00")){
			logReport(LogStatus.PASS,"Verify if no additonal delivery charges are applied","Verification succesfull");
		}else{
			logReport(LogStatus.FAIL,"Verify if no additonal delivery charges are applied","Verification failed");
			captureScreenshot(driver);
		}
		
		//The correct product price is displayed in the users basket
		
		if(cart.checkTotal().equals(prodVal+".00")){
			logReport(LogStatus.PASS,"Verify if total order price is same as product price","Verification succesfull");
		}else{
			logReport(LogStatus.FAIL,"Verify if total order price is same as product price","Verification failed");
			captureScreenshot(driver);
		}
		
		//The user does not need to create an account to checkout, but can continue the purchase as a guest
		
		info=cart.payNow();
		info.enterEmail("abc@gmail.com");
		info.guestAcct();
		if(info.pageTitle(driver).equals("BAG Checkout Page")){
			logReport(LogStatus.PASS,"Verify if user does not need to create an account to checkout, but can continue the purchase as a guest ","Verification succesfull");
		}
		else{
			logReport(LogStatus.FAIL,"Verify if user does not need to create an account to checkout, but can continue the purchase as a guest","Verification failed");
			captureScreenshot(driver);
		}
		//The user can add their Title, Email, First Name, Last Name, Telephone Number etc to the order
		
		bill=info.personalDet("Mrs.", "peter", "quinn", "0899632541");
		if(cart.pageTitle(driver).equals("BAG Checkout Page")){
			logReport(LogStatus.PASS,"Verify if user can add their Title, Email, First Name, Last Name, Telephone Number to the order ","Verification succesfull");
		}
		else{
			logReport(LogStatus.FAIL,"Verify if user can add their Title, Email, First Name, Last Name, Telephone Number to the order ","Verification failed");
			captureScreenshot(driver);
		}
		//User can find their billing address using a post code search
		
		bill.billingdetails("e62nj","2 Norfolk Road, LONDON, E6 2NJ");
		if(bill.pageTitle(driver).equals("BAG Checkout Page")){
			logReport(LogStatus.PASS,"Verify if User can find their billing address using a post code search ","Verification succesfull");
		}
		else{
			logReport(LogStatus.FAIL,"Verify if User can find their billing address using a post code search","Verification failed");
			captureScreenshot(driver);
		}
		
		//When user enters a fake credit card number an error message is displayed
		bill.cardDetails("mkrr", "1234567812345678", "1", "2019", "123");
		if(bill.errMsgMet().equals("The card number is not valid, please check the details and try again.")){
			logReport(LogStatus.PASS,"Verify if invalid card details is displayed","Verification succesfull");
		}else{
			logReport(LogStatus.FAIL,"Verify if invalid card details is displayed","Verification failed");
		}
		}
		
		
	}
	
	
	

