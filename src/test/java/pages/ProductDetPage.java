package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import common.Web;

public class ProductDetPage {

	@FindBy(xpath=".//div[@id='product-price-current']")
	private WebElement currProdPrice;
	
	@FindBy(xpath=".//button[@class='btn btn-transactional top']")
	private WebElement selectBuyNow;
	
	
	private WebDriver driver = null;
	
	public ProductDetPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public String getCurrProdPrice(){
		return Web.getElementValue(currProdPrice);
	}
	
	public CartPage clickBuyNow() {
		Web.clickElement(selectBuyNow);
		return new CartPage(driver);
	}
	
	
}
