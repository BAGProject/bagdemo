package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import common.Web;

public class SupercarExpPage {
	
	@FindBy(xpath=".//div[@class='col-sm-9']//tbody//tr[1]/td[2]/a")
	private WebElement prodLink;
	
	@FindBy(xpath=".//div[@id='crmSignUp']//i")
	private WebElement crmCancel;
	
	private WebDriver driver = null;
	
	public SupercarExpPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public String getProdVal(){
		//if(Web.waitForElement(driver, crmCancel, 5)) Web.clickElement(crmCancel);
		return Web.getElementValue(prodLink);
	}
	
	public  String pageTitle(WebDriver driver){
		return driver.getTitle();
	}
	
	public ProductDetPage selectProduct() {
		//if(Web.waitForElement(driver, crmCancel, 20)) Web.clickElement(crmCancel);
		Web.clickElement(prodLink);
		return new ProductDetPage(driver);
	}

}
