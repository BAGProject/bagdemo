package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import common.Web;

public class HomePage {
	
	@FindBy(xpath="(.//span[text()='Driving'])[3]/..")
	private WebElement menuDriving;
	
	@FindBy(xpath="(.//a[contains(text(),'Supercars')])[5]")
	private WebElement catSupercars;
	
	@FindBy(xpath=".//div[@id='crmSignUp']//i")
	private WebElement crmCancel;
	
	private WebDriver driver = null;
	
	public HomePage(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	public void launchApp(String URL){
		driver.get(URL);
		if(Web.waitForElement(driver, crmCancel, 80)) Web.clickElement(crmCancel);
	}
	
	public  String pageTitle(WebDriver driver){
		return driver.getTitle();
	}
	
	public SupercarExpPage navToSupercarExp(){
	//	if(Web.waitForElement(driver, crmCancel, 20)) Web.clickElement(crmCancel);
		Web.hoverElement(driver, menuDriving);
		Web.waitForElement(driver, catSupercars, 20);
		Web.clickElement(catSupercars);
		return new SupercarExpPage(driver);
	}
	
	

}
