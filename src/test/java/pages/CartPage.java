package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import common.Web;

public class CartPage {

	private WebDriver driver = null;

	@FindBy(xpath = ".//div[@class='form-group col-xs-12 personalised_as_message additional_details_panel']//textarea")
	private WebElement addMessage;

	@FindBy(xpath = ".//select[@ng-model='BasketViewModel.FkDeliveryMethodId']")
	private WebElement eVoucher;

	@FindBy(xpath = ".//button[@class='btn dropdown-toggle buynow pay-secure-now-bottom']")
	private WebElement btnPay;

	@FindBy(xpath = ".//div[@class='row final_totals']//span[2]")
	private WebElement checkTot;

	@FindBy(xpath = ".//div[@class='delivery_totals']//span[2]")
	private WebElement devChgs;
	
	
	public CartPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void addPerMessage(String text) {
		Web.setText(addMessage, text);
	}

	public void addDeliveryMethod(String val) {
		Web.selectDropdownVal(eVoucher, val);
	}

	public  String pageTitle(WebDriver driver){
		return driver.getTitle();
	}
	
	public String getDeliChgsVal() {
		String deliv = Web.getElementValue(devChgs);
		return deliv.replaceAll("[^\\d.]", "");
	}

	public String checkTotal() {
		String deliv = Web.getElementValue(checkTot);
		return deliv.replaceAll("[^\\d.]", "");
	}
	
	public PersnDetPage payNow(){
		Web.clickElement(btnPay);
		Web.handleAlert(driver);
		return new PersnDetPage(driver);
	}

}
