package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import common.Web;

public class PersnDetPage {
	
	private WebDriver driver = null;
	@FindBy(xpath=".//input[@id='account_email_field']")
	private WebElement email;

	@FindBy(xpath=".//button[@class='btn login_guest chk_btns ng-binding']")
	private WebElement btnGuest;
	
	@FindBy(xpath=".//select[@id='titlefield']")
	private WebElement title;
	
	@FindBy(xpath=".//input[@id='firstnamefield']")
	private WebElement firstName;
	
	@FindBy(xpath=".//input[@id='lastnamefield']")
	private WebElement lastName;
	
	@FindBy(xpath=".//input[@id='telephonenumberfield']")
	private WebElement phoneNo;
	
	@FindBy(xpath=".//button[@class='btn login_continue chk_btns pull-right ng-binding']")
	private WebElement btnContinue;
	
	
	public PersnDetPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void enterEmail(String text){
		Web.setText(email, text);
	}
	
	public void guestAcct(){
		Web.clickElement(btnGuest);
	}
	
	public  String pageTitle(WebDriver driver){
		return driver.getTitle();
	}
	
	public BillingAddressPage personalDet(String title_text,String fname,String lname,String pno){
		Web.setText(title, title_text);
		Web.setText(firstName, fname);
		Web.setText(lastName, lname);
		Web.setText(phoneNo, pno);
		Web.clickElement(btnContinue);
		return new BillingAddressPage(driver);
		
		
	}
	
	
	
	
	
}
