package pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import common.Web;

public class BillingAddressPage {
	
	private WebDriver driver=null;
	
	@FindBy(xpath="(.//input[@ng-model='FlowData.PostCodeSearchForm.PostCode'])[2]")
	private WebElement postCode;

	@FindBy(xpath="(.//button[@ng-click='SearchAddressByPostCode($event,true)'])[2]")
	private WebElement btnSearch;
	
	@FindBy(xpath=".//select[@ng-model='AddingBilling.PostCodeResultSelected']")
	private WebElement selAddress;
	
	@FindBy(xpath=".//input[@id='cardholdername']")
	private WebElement name;
	
	@FindBy(xpath=".//input[@id='cardnumber']")
	private WebElement cardno;
	
	@FindBy(xpath=".//select[@id='expirymonth']")
	private WebElement expmonth;
	
	@FindBy(xpath=".//select[@id='expiryyear']")
	private WebElement expyear;
	
	@FindBy(xpath=".//input[@id='cvvnumber']")
	private WebElement cvv;
	
	@FindBy(xpath=".//button[@id='btnPlaceOrderButton']")
	private WebElement btnPlaceOrder;
	
	@FindBy(xpath=".//span[@class='error ng-binding ng-scope']")
	private WebElement invalidErrMsg;
	
	
	
	public BillingAddressPage(WebDriver driver) {
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
		
	public void billingdetails(String pcode,String address){
	
	 try {
			Thread.sleep(9000);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		Web.waitForElement(driver, postCode, 30);
		Web.setText(postCode, pcode);
		Web.clickElement(btnSearch);
		try {
			Thread.sleep(9000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Web.waitForElement(driver, selAddress, 30);
		Web.selectDropdownVal(selAddress, address);
		}
	
	public void cardDetails(String cname,String cno,String mon,String yr,String cv){
		Web.setText(name, cname);
		Web.setText(cardno, cno);
		Web.selectDropdownVal(expmonth, mon);
		Web.selectDropdownVal(expyear, yr);
		Web.setText(cvv, cv);
		Web.clickElement(btnPlaceOrder);
		
	}

	public String errMsgMet(){
		return Web.getElementValue(invalidErrMsg);
	}
	
	public  String pageTitle(WebDriver driver){
		return driver.getTitle();
	}
}
