package common;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Web {

	public static void 	hoverElement(WebDriver driver,WebElement ele){
		Actions act = new Actions(driver);
		act.moveToElement(ele).build().perform();
	}
	
	public static void clickElement(WebElement ele){
		ele.click();
	}
	
	public static String getElementValue(WebElement ele){
		return ele.getText();
			}
	
	public static boolean waitForElement(WebDriver driver, WebElement ele,int waitTime){
		try{
			new WebDriverWait(driver,waitTime).until(ExpectedConditions.elementToBeClickable(ele));
			return true;
		}catch(Exception e){
			return false;
		}
	}
	
	public static void setText(WebElement ele, String text){
		ele.sendKeys(text);
	}
	
	public static void selectDropdownVal(WebElement ele,String val){
		Select sel = new Select(ele);
		sel.selectByVisibleText(val);
	}
	
	public static void handleAlert(WebDriver driver){
		Alert al = driver.switchTo().alert();
		al.accept();
	}
	
	public static String pageTitle(WebDriver driver){
		return driver.getTitle();
	}
	
	public static void takeScreenshot(WebDriver driver,String screenShotName) throws IOException
    {
        TakesScreenshot ts = (TakesScreenshot)driver;
        File source = ts.getScreenshotAs(OutputType.FILE);
        String dest = System.getProperty("user.dir") +"\\ErrorScreenshots\\"+screenShotName+".png";
        File destination = new File(dest);
        FileUtils.copyFile(source, destination);        
                     
    }
		}

