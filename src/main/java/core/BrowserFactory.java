package core;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BrowserFactory {

	private static WebDriver driver = null;
	
	public static WebDriver getDriver(String browName) {
		
		if (driver == null) {
			switch (browName.toLowerCase()) {
			case "chrome":
				System.setProperty("webdriver.chrome.driver", Lib.getProperty("CHROME_DRIVER_PATH"));
				driver = new ChromeDriver();
				break;
			case "firefox":
				System.setProperty("webdriver.firefox.driver", Lib.getProperty("FF_DRIVER_PATH"));
				driver = new ChromeDriver();
				break;

			case "ie":
				break;

			default:
				System.out.println("Invalid browser name provided");
			}
		}
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Long.parseLong(Lib.getProperty("IMPLICIT_WAIT")), TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(Long.parseLong(Lib.getProperty("PAGE_LOAD_TIMEOUT")), TimeUnit.SECONDS);

		return driver;
	}
	
	public static void quitDriver() {
		if(driver != null) driver.quit();
		driver = null;
	}
	
}
