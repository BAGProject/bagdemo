package core;

import java.io.File;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Reporter {
	
	ExtentReports extReport = null;
	ExtentTest extTest = null;
	
	public Reporter() {
		File file = new File(System.getProperty("user.dir")+"//TestReport");
		if(!file.exists()) file.mkdir();
		extReport = new ExtentReports(System.getProperty("user.dir")+"//TestReport//HTMLReport.html",true);
	}
	
	public void StartTest(String testName) {
		extTest = extReport.startTest(testName);
	}
	
	public void Log(LogStatus status,String testStep, String stepDetails) {
		extTest.log(status, testStep, stepDetails);
	}
	
	public void EndTest() {
		extReport.endTest(extTest);
	}
	
	public void FlushTest() {
		extReport.flush();
	}
	
	public void CleanTest() {
		extReport.close();
	}
	
	
	

}
